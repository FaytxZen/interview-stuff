import java.util.*;

public class BucketSort {
    
    public static void bucketSort(int[] arr) {
        if(arr.length < 1) return;
        
        int max = Integer.MIN_VALUE;
        
        for(int num : arr) {
            if(num > max) {
                max = num;
            }
        }
        
        int[] copyArr = new int[max+1];
        
        for(int num : arr) {
            copyArr[num]++;
        }
        
        for(int i = 0, j = 0; i < copyArr.length && j < arr.length; i++) {
            while(copyArr[i]-- > 0) {
                arr[j++] = i;
            }
        }
    }
    
    public static void main(String[] args) {
        // case 1
        int[] arr1 = {4, 3, 2, 9, 12 };
        bucketSort(arr1);
        
        assert Arrays.equals(arr1, new int[] { 2, 3, 4, 9, 12 });

        // case 2
        int[] arr2 = { 100, 5, 6, 0, 2, 1};
        bucketSort(arr2);
        
        assert Arrays.equals(arr2, new int[] { 0, 1, 2, 5, 6, 100 });
        
        // case 3
        int[] arr3 = { 1 };
        bucketSort(arr3);
        
        assert Arrays.equals(arr3, new int[] { 1 });
        
        // case 4
        int[] arr4 = {};
        bucketSort(arr4);
        
        assert Arrays.equals(arr4, new int[0]);
        
        // case 5
        int[] arr5 = { 1, 2, 3 };
        bucketSort(arr5);
        
        assert Arrays.equals(arr5, new int[] { 1, 2, 3 });
    }
}