import java.util.*;

public class BubbleSort {
    
    public static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    } 
    
    public static void bubbleSort(int[] arr) {
        for(int i = 0; i < arr.length; i++) {
            for(int j = 1; j < arr.length - i; j++) {
                
                if(arr[j-1] > arr[j]) {
                    swap(arr, j-1, j);
                }
            }
        }
    }
    
    public static void main(String[] args) {
        // case 1
        int[] arr1 = {4, 3, 2, 9, 12 };
        bubbleSort(arr1);
        
        assert Arrays.equals(arr1, new int[] { 2, 3, 4, 9, 12 });

        // case 2
        int[] arr2 = { -1, 5, 6, 0, 2, 1};
        bubbleSort(arr2);
        
        assert Arrays.equals(arr2, new int[] { -1, 0, 1, 2, 5, 6 });
        
        // case 3
        int[] arr3 = { 1 };
        bubbleSort(arr3);
        
        assert Arrays.equals(arr3, new int[] { 1 });
        
        // case 4
        int[] arr4 = {};
        bubbleSort(arr4);
        
        assert Arrays.equals(arr4, new int[0]);
        
        // case 5
        int[] arr5 = { 1, 2, 3 };
        bubbleSort(arr5);
        
        assert Arrays.equals(arr5, new int[] { 1, 2, 3 });
    }
}