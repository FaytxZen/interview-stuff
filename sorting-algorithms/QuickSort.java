import java.util.*;

/**
 * TODO: fix this 
 */
public class QuickSort {
    
    public static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
    
    public static int partition(int[] arr, int pivot, int left, int right) {
        int pivotEl = arr[pivot];
        
        for(int i = left, j = right; i <= j;) {
            
            while(arr[i] < pivotEl) {
                i++;
            }
            
            while(arr[j] > pivotEl) {
                j--;
            }
            
            System.out.println(String.format("i=%d, j=%d, pivot=%d", i, j, pivot));
            
            if(i <= j) {
                swap(arr, i, j);
                left = ++i;
                j--;
            }
        }
        
        return left;
    }
    
    public static void quickSort(int[] arr, int pivot, int left, int right) {
        
        int newPivot = partition(arr, pivot, left, right);
        
        if(left < pivot - 1) {
            quickSort(arr, newPivot, left, pivot-1);
        }
        
        if(pivot + 1 < right) {
            quickSort(arr, newPivot, pivot+1, right);    
        }
    }
    
    public static void quickSort(int[] arr) {
        if(arr.length > 1) {
            int pivot = new Random().nextInt(arr.length) % arr.length;
            quickSort(arr, pivot, 0, arr.length-1);    
        }
    }
    
    public static void main(String[] args) {
        // case 1
        int[] arr1 = {4, 3, 2, 9, 12 };
        quickSort(arr1);
        
        assert Arrays.equals(arr1, new int[] { 2, 3, 4, 9, 12 });

        // case 2
        int[] arr2 = { -1, 5, 6, 0, 2, 1};
        quickSort(arr2);
        
        System.out.println(Arrays.toString(arr2));
        assert Arrays.equals(arr2, new int[] { -1, 0, 1, 2, 5, 6 });
        
        // case 3
        int[] arr3 = { 1 };
        quickSort(arr3);
        
        assert Arrays.equals(arr3, new int[] { 1 });
        
        // case 4
        int[] arr4 = {};
        quickSort(arr4);
        
        assert Arrays.equals(arr4, new int[0]);
        
        // case 5
        int[] arr5 = { 1, 2, 3 };
        quickSort(arr5);
        
        assert Arrays.equals(arr5, new int[] { 1, 2, 3 });
    }
}