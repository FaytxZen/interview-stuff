import java.util.*;

public class MergeSort {
    
    public static void merge(int[] arr, int[] temp, int left, int middle, int right) {
        for(int i = left; i <= right; i++) {
            temp[i] = arr[i];
        }
        
        int i = left;
        int j = middle + 1;
        int k = left;
        
        // copy the smallest values
        while(i <= middle && j <= right) {
            if(temp[i] <= temp[j]) {
                arr[k] = temp[i++];
            }
            else {
                arr[k] = temp[j++];
            }
            
            k++;
        }
        
        while(i <= middle) {
            arr[k++] = temp[i++];
        }
    }
    
    public static void mergeSort(int[] arr, int[] temp, int left, int right) {
        if(left < right) {
            int middle = left + (right - left) / 2;
            
            mergeSort(arr, temp, left, middle);
            mergeSort(arr, temp, middle + 1, right);
            
            merge(arr, temp, left, middle, right);
        }
    }
    
    public static void mergeSort(int[] arr) {
        if(arr.length > 1) {
            mergeSort(arr, new int[arr.length], 0, arr.length-1);
        }
    }
    
    public static void main(String[] args) {
        // case 1
        int[] arr1 = {4, 3, 2, 9, 12 };
        mergeSort(arr1);
        
        assert Arrays.equals(arr1, new int[] { 2, 3, 4, 9, 12 });

        // case 2
        int[] arr2 = { -1, 5, 6, 0, 2, 1};
        mergeSort(arr2);
        
        assert Arrays.equals(arr2, new int[] { -1, 0, 1, 2, 5, 6 });
        
        // case 3
        int[] arr3 = { 1 };
        mergeSort(arr3);
        
        assert Arrays.equals(arr3, new int[] { 1 });
        
        // case 4
        int[] arr4 = {};
        mergeSort(arr4);
        
        assert Arrays.equals(arr4, new int[0]);
        
        // case 5
        int[] arr5 = { 1, 2, 3 };
        mergeSort(arr5);
        
        assert Arrays.equals(arr5, new int[] { 1, 2, 3 });
    }
}