import java.util.*;

public class SelectionSort {
    
    public static void swap(int[] arr, int i, int j) {
        int temp = arr[j];
        arr[j] = arr[i];
        arr[i] = temp;
    }
    
    public static void selectionSort(int[] arr) {
        for(int i = 0; i < arr.length - 1; i++) {
            int smallest = i;
            
            for(int j = i + 1; j < arr.length; j++) {
                if(arr[smallest] > arr[j]) {
                    smallest = j;
                }
            }
            
            swap(arr, i, smallest);
        }
    }
    
    public static void main(String[] args) {
        // case 1
        int[] arr1 = {4, 3, 2, 9, 12 };
        selectionSort(arr1);
        
        assert Arrays.equals(arr1, new int[] { 2, 3, 4, 9, 12 });

        // case 2
        int[] arr2 = { -1, 5, 6, 0, 2, 1};
        selectionSort(arr2);
        
        assert Arrays.equals(arr2, new int[] { -1, 0, 1, 2, 5, 6 });
        
        // case 3
        int[] arr3 = { 1 };
        selectionSort(arr3);
        
        assert Arrays.equals(arr3, new int[] { 1 });
        
        // case 4
        int[] arr4 = {};
        selectionSort(arr4);
        
        assert Arrays.equals(arr4, new int[0]);
        
        // case 5
        int[] arr5 = { 1, 2, 3 };
        selectionSort(arr5);
        
        assert Arrays.equals(arr5, new int[] { 1, 2, 3 });
    }
}