import java.util.*;

public class InsertionSort {
    
    public static void insertionSort(int[] arr) {
        for(int i = 1; i < arr.length; i++) {
            int temp = arr[i];
            int j = i;
            
            while(j > 0 && arr[j-1] > temp) {
                arr[j] = arr[j-1];
                j--;
            }
            
            arr[j] = temp;
        }
    }
    
    public static void main(String[] args) {
        // case 1
        int[] arr1 = {4, 3, 2, 9, 12};
        insertionSort(arr1);
        
        assert Arrays.equals(arr1, new int[] { 2, 3, 4, 9, 12 });

        // case 2
        int[] arr2 = { -1, 5, 6, 0, 2, 1};
        insertionSort(arr2);
        
        assert Arrays.equals(arr2, new int[] { -1, 0, 1, 2, 5, 6 });
        
        // case 3
        int[] arr3 = { 1 };
        insertionSort(arr3);
        
        assert Arrays.equals(arr3, new int[] { 1 });
        
        // case 4
        int[] arr4 = {};
        insertionSort(arr4);
        
        assert Arrays.equals(arr4, new int[0]);
        
        // case 5
        int[] arr5 = { 1, 2, 3 };
        insertionSort(arr5);
        
        assert Arrays.equals(arr5, new int[] { 1, 2, 3 });
    }
}