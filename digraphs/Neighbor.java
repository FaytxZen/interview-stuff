import java.lang.*;

public class Neighbor implements Comparable<Neighbor> {
    
    private Object to;
    private int weight;
    
    public Neighbor(Object to, int weight) {
        this.to = to;
        this.weight = weight;
    }
    
    public Object value() {
        return to;
    }
    
    public int weight() {
        return weight;
    }
    
    @Override
    public boolean equals(Object obj) {
        return  obj instanceof Neighbor && 
                to.equals(((Neighbor) obj).value()) && 
                weight == ((Neighbor) obj).weight();
    }
    
    @Override
    public int compareTo(Neighbor n) {
        if(weight < n.weight()) {
            return -1;
        }
        else if(weight > n.weight()) {
            return 1;
        }
        else {
            return 0;
        }
    }
}