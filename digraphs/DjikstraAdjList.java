import java.util.*;

public class DjikstraAdjList {
    
    public static final int INF = Integer.MAX_VALUE;
    
    public static Neighbor[] shortestPaths(AdjacencyList graph, Object source, Object[] vertices) {
        Map<Object, Integer> distances = new LinkedHashMap<Object, Integer>();
        
        for(Object obj : vertices) {
            distances.put(obj, obj.equals("0") ? 0 : INF);
        }
        
        PriorityQueue<Neighbor> priorityQueue = new PriorityQueue<Neighbor>(graph.size(), new Comparator<Neighbor>() {
            @Override
            public int compare(Neighbor n1, Neighbor n2) {
                return n1.compareTo(n2);                
            }
        });
        
        priorityQueue.add(new Neighbor("0", 0));
        
        while(priorityQueue.size() > 0) {
            final Neighbor current = priorityQueue.poll();
            final int currentDist = current.weight();
            
            List<Neighbor> neighbors = graph.getNeighbors(current.value());
            
            for(Neighbor n : neighbors) {
                final int proposedDist = n.weight() + currentDist;
                final boolean shorter = distances.get(n.value()) > proposedDist;
                
                if(shorter) {
                    distances.put(n.value(), proposedDist);
                    priorityQueue.add(new Neighbor(n.value(), proposedDist));
                }
            }
        }
        
        Neighbor[] shortestPaths = new Neighbor[graph.size()];
        int index = 0;
        for(Object key : distances.keySet()) {
            shortestPaths[index++] = new Neighbor(key, distances.get(key));
        }
        return shortestPaths;
    }
    
    public static void main(String[] args) {
        AdjacencyList graph = new AdjacencyList();
        graph.addEdge("0", "1", 5);
        graph.addEdge("0", "3", 2);
        graph.addEdge("1", "5", 1);
        graph.addEdge("2", "7", 1);
        graph.addEdge("3", "1", 2);
        graph.addEdge("3", "2", 1);
        graph.addEdge("5", "2", 4);
        graph.addEdge("7", "5", 2);
        graph.addEdge("7", "0", 8);
        
        Neighbor[] neighbors = shortestPaths(graph, "0", new String[] {"0", "1", "2", "3", "5", "7"});
        
        for(Neighbor neighbor : neighbors) {
            
            if(neighbor.value().equals("0")) {
                assert neighbor.equals(new Neighbor("0", 0));
            }
            else if(neighbor.value().equals("1")) {
                assert neighbor.equals(new Neighbor("1", 4));
            }
            else if(neighbor.value().equals("2")) {
                assert neighbor.equals(new Neighbor("2", 3));
            }
            else if(neighbor.value().equals("3")) {
                assert neighbor.equals(new Neighbor("3", 2));
            }
            else if(neighbor.value().equals("5")) {
                assert neighbor.equals(new Neighbor("5", 5));
            }
            else if(neighbor.value().equals("7")) {
                assert neighbor.equals(new Neighbor("7", 4));
            }
            else {
                assert false;
            }
        }
    }
}