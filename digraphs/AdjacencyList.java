import java.util.*;

public class AdjacencyList {
    
    private Map<Object, List<Neighbor>> map = new LinkedHashMap<Object, List<Neighbor>>();
    
    public void addEdge(Object from, Object to, int weight) {
        if(!map.containsKey(from)) {
            List<Neighbor> neighbors = new ArrayList<Neighbor>();
            map.put(from, neighbors);
        }
        
        Neighbor neighbor = new Neighbor(to, weight);
        map.get(from).add(neighbor);
    }
    
    public void removeEdge(Object from, Object to) {
        if(map.containsKey(from)) {
            List<Neighbor> neighbors = map.get(from);
            
            for(Neighbor neighbor : neighbors) {
                if(neighbor.value().equals(to)) {
                    neighbors.remove(neighbor);
                    break;
                }
            }
        }
    }
    
    public List<Neighbor> getNeighbors(Object source) {
        return map.get(source);
    }
    
    public boolean hasEdge(Object from, Object to) {
        if(map.containsKey(from)) {
            List<Neighbor> neighbors = map.get(from);
            
            for(Neighbor neighbor : neighbors) {
                if(neighbor.value().equals(to)) {
                    return true;
                }
            }
        }
        
        return false;
    }
    
    public int costOf(Object from, Object to) {
        if(map.containsKey(from)) {
            List<Neighbor> neighbors = map.get(from);
            
            for(Neighbor neighbor : neighbors) {
                if(neighbor.value().equals(to)) {
                    return neighbor.weight();
                }
            }
        }
        
        return Integer.MAX_VALUE;
    }
    
    public int size() {
        return map.size();
    }

    public static void main(String... args) {
        AdjacencyList adjList = new AdjacencyList();
        
        adjList.addEdge(0, 1, 1);
        adjList.addEdge(6, 2, 100);
        adjList.addEdge(0, 6, 5);
        
        assert adjList.hasEdge(0, 1);
        assert adjList.hasEdge(0, 6);
        assert adjList.hasEdge(6, 2);
        
        adjList.removeEdge(0, 1);
        
        assert !adjList.hasEdge(0, 1);
        
        assert adjList.costOf(6, 2) == 100;
        assert adjList.costOf(0, 6) == 5;
        assert adjList.costOf(2, 0) == Integer.MAX_VALUE;
    }    
}