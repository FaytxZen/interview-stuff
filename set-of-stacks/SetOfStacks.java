import java.util.*;

/**
 * Problem: <br/>
 * 
 * Imagine a (literal) stack of plates. IF the stack gets too high, it might topple.
 * Therefore, in real life, we would likely start a new stack when the previous
 * stack exceeds some threshold. Implement a data structure SetOfStacks that
 * mimics this.
 * SetOfStacks should be composed of several stacks and should create a new stack
 * once the previous one exceeds capacity. SetOfStacks.push() and SetOfStacks.pop()
 * should behave identically to a single stack (that is, pop() should return the
 * same values as it would if it were just a single stack).
 * 
 * FOLLOW UP <br/>
 * Implement a function popAt(int index) which performs a pop operations on a specific
 * sub-stack.
 */
public class SetOfStacks<T> {
    
    private int limit = 1;
    private int size;
    private List<Stack<T>> stacks = new ArrayList<Stack<T>>();
    
    public SetOfStacks(int limit) {
        if(limit < 1) {
            limit = 1;
        }
        
        this.limit = limit;
        this.size = 0;
    }
    
    public void push(T data) {
        final int numStacks = numberOfStacks();
        final boolean noStacksInList = numStacks == 0;
        final boolean needNewStack = noStacksInList || size(numStacks-1) == limit;
        
        if(needNewStack) {
            Stack<T> stack = new Stack<T>();
            stack.add(data);
            
            stacks.add(stack);
        }
        else {
            stacks.get(stacks.size() - 1).add(data);
        }
        
        size++;
    }
    
    public T pop(int stack) {
        T data = stacks.get(stack).pop();
        
        if(stacks.get(stack).size() == 0) {
            stacks.remove(stack);
        }
        
        size--;
        
        return data;
    }
    
    public T pop() {
        int stack = stacks.size() - 1;
        
        T data = stacks.get(stack).pop();
        
        if(stacks.get(stack).size() == 0) {
            stacks.remove(stack);
        }
        
        size--;
        
        return data;
    }
    
    public int size() {
        return size;
    }
    
    public int size(int stack) {
        return stacks.get(stack).size();
    }
    
    public int numberOfStacks() {
        return stacks.size();
    }
    
    public static void main(String[] args) {
        SetOfStacks<Integer> stackSet = new SetOfStacks<Integer>(3);
        
        stackSet.push(1);
        stackSet.push(2);
        stackSet.push(3);
        stackSet.push(4);
        stackSet.push(5);
        
        assert stackSet.size() == 5;
        assert stackSet.pop() == 5;
        assert stackSet.pop(0) == 3;
        assert stackSet.size(0) == 2;
        assert stackSet.numberOfStacks() == 2;
    }
}