#!/bin/bash

# global variables
tempfile="_deleteme_testfile.txt"

# find all java files in directory
find -name "*.java" > $tempfile

# compile and run each file
while read -r line
do
    # echo "Testing $line\n"
    
    if [[ ${line} == *".java"* ]]
    then
        dir=$(dirname $line)
        file=$(basename $line)
        filename="${file%.*}"
        
        cd $dir
        
        javac $file
        java -ea $filename
        
        cd ~/workspace/interview
    fi

done < $tempfile

# delete created files
rm $tempfile
rm **/*.class > /dev/null