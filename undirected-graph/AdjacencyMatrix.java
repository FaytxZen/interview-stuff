import java.util.*;

public class AdjacencyMatrix {
    private int[][] matrix;

    public AdjacencyMatrix(int n) {
        matrix = new int[n][n];
    }
    
    public void addEdge(int u, int v) {
        matrix[u][v]++;
        matrix[v][u]++;
    }
    
    public void removeEdge(int u, int v) {
        if(matrix[u][v] > 0) {
            matrix[u][v]--;
        }
        
        if(matrix[v][u] > 0) {
            matrix[v][u]--;
        }
    }
    
    public boolean hasEdge(int u, int v) {
        return matrix[u][v] > 0 && matrix[v][u] > 0;
    }
    
    public String dfs() {
        Stack stack = new Stack();
        stack.push(0);
        
        String result = stack.peek() + " ";
        
        boolean[] visited = new boolean[matrix.length];
        
        while(!stack.empty()) {
            int cur = (Integer) stack.peek();
            
            visited[cur] = true;
            
            boolean added = false;
            
            for(int i = 0; i < matrix.length; i++) {
                if(matrix[cur][i] > 0 && !visited[i]) {
                    result += i + " ";
                    stack.push(i);
                    added = true;
                    break;    
                }
            }
            
            if(!added) {
                stack.pop();
            }
        }
        
        return result;
    }
    
    public String bfs() {
        Queue<Integer> queue = new LinkedList<Integer>();
        queue.add(0);
        
        String result = queue.peek() + " ";
        boolean[] visited = new boolean[matrix.length];
        visited[queue.peek()] = true;
        
        while(queue.peek() != null) {
            int cur = queue.remove();
            
            for(int i = 0; i < matrix.length; i++) {
                if(matrix[cur][i] > 0 && !visited[i]) {
                    queue.add(i);
                    result += i + " ";
                    
                    visited[i] = true;
                }
            }
        }
        
        return result;
    }
    
    public static void main(String... args) {
        
        AdjacencyMatrix matrix = new AdjacencyMatrix(10);
        matrix.addEdge(0, 1);
        matrix.addEdge(0, 4);
        matrix.addEdge(1, 6);
        matrix.addEdge(1, 5);
        matrix.addEdge(1, 7);
        matrix.addEdge(2, 5);
        matrix.addEdge(2, 4);
        matrix.addEdge(3, 6);
        matrix.addEdge(3, 7);
        matrix.addEdge(4, 5);
        matrix.addEdge(4, 8);
        matrix.addEdge(5, 9);
        matrix.addEdge(8, 9);
        
        assert "0 1 4 5 6 7 2 8 9 3".equals(matrix.bfs().trim());
        assert "0 1 5 2 4 8 9 6 3 7".equals(matrix.dfs().trim());
        
        assert matrix.hasEdge(0, 1);
        matrix.removeEdge(0, 1);
        assert !matrix.hasEdge(1, 0);
        
        matrix.removeEdge(5, 9);
        assert !matrix.hasEdge(5, 9);
    }
}