import java.util.*;

public class WeightedAdjacencyMatrix {
    public static final int INF = Integer.MAX_VALUE / 2;
    
    private int[][] matrix;
    
    public WeightedAdjacencyMatrix(int n) {
        matrix = new int[n][n];
        
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                matrix[i][j] = i == j ? 0 : INF;
            }
        }
    }
    
    public void addEdge(int u, int v, int weight) {
        matrix[u][v] = weight;
        matrix[v][u] = weight;
    }
    
    public void removeEdge(int u, int v) {
        matrix[u][v] = u == v ? 0 : INF;
        matrix[v][u] = u == v ? 0 : INF;
    }
    
    public boolean hasEdge(int u, int v) {
        return matrix[u][v] < INF && matrix[v][u] < INF;
    }
    
    public String bfs() {
        boolean[] visited = new boolean[matrix.length];
        visited[0] = true;
        
        Queue<Integer> queue = new LinkedList<Integer>();
        queue.add(0);
        
        String result = 0 + " ";
        
        while(queue.peek() != null) {
            
            int cur = queue.remove();
            
            for(int i = 0; i < matrix.length; i++) {
                if(hasEdge(cur, i) && !visited[i]) {
                    result += i + " ";
                    visited[i] = true;
                    
                    queue.add(i);
                }    
            }
        }
        
        return result;
    }
    
    public String dfs() {
        boolean[] visited = new boolean[matrix.length];
        visited[0] = true;
        
        Stack stack = new Stack();
        stack.push(0);
        
        String result = 0 + " ";
        
        while(!stack.empty()) {
            int cur = (Integer) stack.peek();
            
            for(int i = 0; i < matrix.length; i++) {
                if(hasEdge(cur, i) && !visited[i]) {
                    stack.push(i);
                    visited[i] = true;
                    result += i + " ";
                    break;
                }    
            }
            
            if((Integer) stack.peek() == cur) {
                stack.pop();
            }
        }
        
        return result;
    }
    
    public int[][] floydWarshall() {
        int n = matrix.length;
        int[][] result = new int[n][n];
        
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                result[i][j] = matrix[i][j];
            }
        }
        
        for(int i = 0; i < n; i++) {
            for(int j = 0; j < n; j++) {
                for(int k = 0; k < n; k++) {
                    result[j][k] = Math.min(result[j][k], result[j][i] + result[i][k]);
                }
            }
        }
        
        return result;
    }
    
    public static void main(String[] args) {
        WeightedAdjacencyMatrix adjMatrix = new WeightedAdjacencyMatrix(6);
        
        adjMatrix.addEdge(0, 1, 8);
        adjMatrix.addEdge(0, 5, 3);
        adjMatrix.addEdge(1, 4, 2);
        adjMatrix.addEdge(2, 5, 2);
        adjMatrix.addEdge(3, 4, 5);
        adjMatrix.addEdge(5, 4, 1);
        
        assert "0 1 5 4 2 3".equals(adjMatrix.bfs().trim());
        assert "0 1 4 3 5 2".equals(adjMatrix.dfs().trim());
        
        int[][] shortestPaths = adjMatrix.floydWarshall();
        
        int[][] expected = {
            {0, 6, 5, 9, 4, 3},
            {6, 0, 5, 7, 2, 3},
            {5, 5, 0, 8, 3, 2},
            {9, 7, 8, 0, 5, 6},
            {4, 2, 3, 5, 0, 1},
            {3, 3, 2, 6, 1, 0}
        };
        
        for(int i = 0; i < 6; i++) {
            for(int j = 0; j < 6; j++) {
                assert shortestPaths[i][j] == expected[i][j];
            }
        }
    }
}