import java.util.*;

public class AdjacencyList {
    
    private Map<Integer, List<Integer>> adjList = new LinkedHashMap<Integer, List<Integer>>();
    
    public void addEdge(int u, int v) {
        if(adjList.containsKey(u)) {
            adjList.get(u).add(v);
        }
        else {
            List<Integer> list = new ArrayList<Integer>();
            list.add(v);
            
            adjList.put(u, list);
        }
        
        if(adjList.containsKey(v)) {
            adjList.get(v).add(u);
        }
        else {
            List<Integer> list = new ArrayList<Integer>();
            list.add(u);
            
            adjList.put(v, list);
        }
    }
    
    public void removeEdge(int u, int v) {
        if(adjList.containsKey(u)) {
            List<Integer> list = adjList.get(u);
            list.remove(list.indexOf(v));
        }
        
        if(adjList.containsKey(v)) {
            List<Integer> list = adjList.get(v);
            list.remove(list.indexOf(u));
        }
    }
    
    public boolean hasEdge(int u, int v) {
        return 
            adjList.containsKey(u) && adjList.get(u).contains(v) &&
            adjList.containsKey(v) && adjList.get(v).contains(u);
    }
    
    public String dfs() {
        Stack stack = new Stack();
        stack.push(0);
        
        String result = 0 + " ";
        boolean[] visited = new boolean[adjList.size()];
        visited[0] = true;
        
        while(!stack.empty()) {
            List<Integer> list = adjList.get(stack.peek());
            boolean added = false;
            
            for(int i = 0; i < list.size(); i++) {
                int neighbor = list.get(i);
                
                if(!visited[neighbor]) {
                    stack.push(neighbor);
                    
                    result += neighbor + " ";
                    
                    visited[neighbor] = true;
                    added = true;
                    break;
                }
            }
            
            if(!added) {
                stack.pop();
            }
        }
        
        return result;
    }
    
    public String bfs() {
        Queue<Integer> queue = new LinkedList<Integer>();
        queue.add(0);
        
        String result = 0 + " ";
        boolean[] visited = new boolean[adjList.size()];
        visited[0] = true;
        
        while(queue.peek() != null) {
            int cur = queue.remove();
            List<Integer> list = adjList.get(cur);
            
            for(int i = 0; list != null && i < list.size(); i++) {
                int neighbor = list.get(i);
                
                if(!visited[neighbor]) {
                    result += neighbor + " ";
                    queue.add(neighbor);
                    visited[neighbor] = true;
                }
            }
        }
        
        return result;
    }
    
    public static void main(String... args) {
        AdjacencyList adjList = new AdjacencyList();
        adjList.addEdge(0, 1);
        adjList.addEdge(0, 4);
        adjList.addEdge(1, 5);
        adjList.addEdge(1, 6);
        adjList.addEdge(1, 7);
        adjList.addEdge(2, 4);
        adjList.addEdge(2, 5);
        adjList.addEdge(3, 6);
        adjList.addEdge(3, 7);
        adjList.addEdge(4, 5);
        adjList.addEdge(4, 8);
        adjList.addEdge(5, 9);
        adjList.addEdge(8, 9);
        
        assert "0 1 4 5 6 7 2 8 9 3".equals(adjList.bfs().trim());
        assert "0 1 5 2 4 8 9 6 3 7".equals(adjList.dfs().trim());
        
        assert adjList.hasEdge(0, 1);
        adjList.removeEdge(0, 1);
        assert !adjList.hasEdge(1, 0);
        
        adjList.removeEdge(5, 9);
        assert !adjList.hasEdge(5, 9);
    }
}