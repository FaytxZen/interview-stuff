
import java.util.*;

/**
 * In the classic problem, Towers of Hanoi, you have 3 rods and N disks of
 * different sizes which can slide onto any tower. The puzzle starts with
 * disks sorted in ascending order of size from top to bottom.
 * You have the following constraints:
 * 
 * - Only one disk can be moved at a time.
 * - A disk is slid off the top of one rod onto the next rod.
 * - A disk can only be placed on top of a larger disk.
 * 
 * Write a program to move the disks from the first rod to the last using Stacks.
 */
public class TowersOfHanoi {
    
    public static Stack<Integer> towersOfHanoi(int n) {
        Stack<Integer> rod1 = new Stack<Integer>();
        Stack<Integer> rod2 = new Stack<Integer>();
        Stack<Integer> rod3 = new Stack<Integer>();
        
        for(int i = 0; i < n; i++) {
            rod1.push(n - i);
        }
        
        towersOfHanoi(rod1, rod3, rod2, n);
        
        return rod3;
    }
    
    public static void towersOfHanoi(
                                Stack<Integer> start, 
                                Stack<Integer> target, 
                                Stack<Integer> spare,
                                int numToMove) 
    {
        if(numToMove == 1) {
            target.push(start.pop());
            towersOfHanoi(start, target, spare, numToMove-1);
        }
        else if(numToMove > 1) {
            towersOfHanoi(start, spare, target, numToMove-1);
            target.push(start.pop());
            towersOfHanoi(spare, target, start, numToMove-1);
        }
    }
    
    public static void main(String[] args) {
        
        Stack<Integer> sol1 = towersOfHanoi(3);
        
        int prev = sol1.pop();
        while(!sol1.empty()) {
            assert prev < sol1.peek();
            prev = sol1.pop();
        }
        
        Stack<Integer> sol2 = towersOfHanoi(12);
        prev = sol2.pop();
        while(!sol2.empty()) {
            assert prev < sol2.peek();
            prev = sol2.pop();
        }
        
        Stack<Integer> sol3 = towersOfHanoi(20);
        prev = sol3.pop();
        while(!sol3.empty()) {
            assert prev < sol3.peek();
            prev = sol3.pop();
        }
    }
}