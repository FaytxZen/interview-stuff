import java.util.*;

/**
 * Implement a StackQueue class which implements a queue using two stacks.
 */
public class StackQueue {
    
    Stack<Integer> s1 = new Stack<Integer>();
    Stack<Integer> s2 = new Stack<Integer>();
    
    public void enqueue(int data) {
        if(s1.empty()) {
            s2.push(data);
        }
        else {
            s1.push(data);            
        }
    }
    
    public int dequeue() {
        int result = Integer.MIN_VALUE;
        
        if(s1.empty()) {
            
            while(!s2.empty()) {
                result = s2.pop();
                s1.push(result);
            }
            
            s1.pop();
            
            while(!s1.empty()) {
                s2.push(s1.pop());
            }
        }
        else {
            while(!s1.empty()) {
                result = s1.pop();
                s2.push(result);
            }
            
            s2.pop();
            
            while(!s2.empty()) {
                s1.push(s2.pop());
            }
        }
        
        return result;
    }
    
    public int peek() {
        int result = Integer.MIN_VALUE;
        
        if(s1.empty()) {
            
            while(!s2.empty()) {
                result = s2.pop();
                s1.push(result);
            }
            
            while(!s1.empty()) {
                s2.push(s1.pop());
            }
        }
        else {
            while(!s1.empty()) {
                result = s1.pop();
                s2.push(result);
            }
            
            while(!s2.empty()) {
                s1.push(s2.pop());
            }
        }
        
        return result;
    }
    
    public static void main(String[] args) {
        StackQueue queue = new StackQueue();
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        
        assert queue.dequeue() == 1;
        
        int peek = queue.peek();
        int dequeue = queue.dequeue();
        
        assert peek == dequeue;
    }
}