import java.util.*;

public class EvenOddArraySlow {
    
    public static void sortEvensAtFront(int[] arr) {
        if(arr.length < 2) return;
        
        for(int i = 0; i < arr.length; i++) {
            for(int j = 0; j < arr.length - 1; j++) {
                boolean evenAndLess = arr[j] % 2 == 0 && arr[j] > arr[j+1];
                boolean evenAndPrevIsOdd = arr[j+1] % 2 == 0 && arr[j] % 2 == 1;
                
                if(evenAndLess || evenAndPrevIsOdd) {
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
            }
        }
    }
    
    public static void main(String[] args) {
       int[] case1 = { 1, 2, 0, 3, 5, 4, 12, 10 };
       sortEvensAtFront(case1);
       System.out.println(Arrays.toString(case1));
       
       
       int[] case2 = { 2, 4, 6, 5 };
       sortEvensAtFront(case2);
       System.out.println(Arrays.toString(case2));
       
       int[] case3 = { 1, 2 };
       sortEvensAtFront(case3);
       System.out.println(Arrays.toString(case3));
       
       int[] case4 = { 1 };
       sortEvensAtFront(case4);
       System.out.println(Arrays.toString(case4));
    }
}