import java.util.*;

public class EvenOddArrayFast {
    
    public static void sortEvensAtFront(int[] arr) {
        int nextEven = 0;
        int nextOdd = arr.length - 1;
        
        while(nextEven < nextOdd) {
            if(arr[nextEven] % 2 == 0) {
                nextEven++;
            }
            else {
                int temp = arr[nextEven];
                arr[nextEven] = arr[nextOdd]; 
                arr[nextOdd--] = temp;
            }
        }
    }
    
    public static void main(String[] args) {
        int[] case1 = { 1, 2, 0, 3, 5, 4, 12, 10 };
       sortEvensAtFront(case1);
       System.out.println(Arrays.toString(case1));
       
       
       int[] case2 = { 2, 4, 6, 5 };
       sortEvensAtFront(case2);
       System.out.println(Arrays.toString(case2));
       
       int[] case3 = { 1, 2 };
       sortEvensAtFront(case3);
       System.out.println(Arrays.toString(case3));
       
       int[] case4 = { 1 };
       sortEvensAtFront(case4);
       System.out.println(Arrays.toString(case4));
    }
}