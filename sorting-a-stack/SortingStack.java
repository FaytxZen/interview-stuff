
import java.util.*;

/**
 * Write a program to sort a stack in ascending order.
 * You should not make any assumptions about how the stack is implemented.
 * The following are the only functions that should be used to write this
 * program: push | pop | peek | isEmpty.
 */ 
public class SortingStack {
    
    public static void sortStack(Stack<Integer> stack) {
        Stack<Integer> temp = new Stack<Integer>();
        
        while(!stack.empty()) {
            int tmp = stack.pop();
            
            while(!temp.empty() && temp.peek() > tmp) {
                stack.push(temp.pop());
            }
            
            temp.push(tmp);
        }
        
        while(!temp.empty()) {
            stack.push(temp.pop());
        }
    }
    
    public static void main(String[] args) {
        Stack<Integer> stack1 = new Stack<Integer>();
        stack1.push(1);
        stack1.push(-1);
        
        sortStack(stack1);
        assert stack1.pop() == -1;
        assert stack1.pop() == 1;
        
        Stack<Integer> stack2 = new Stack<Integer>();
        stack2.push(5);
        stack2.push(0);
        stack2.push(100);
        stack2.push(45);
        
        sortStack(stack2);
        assert stack2.pop() == 0;
        assert stack2.pop() == 5;
        assert stack2.pop() == 45;
        assert stack2.pop() == 100;
    }
}