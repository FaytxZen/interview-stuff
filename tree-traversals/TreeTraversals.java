import java.util.*;

public class TreeTraversals {
    
    public static class TreeNode<T> {
        private T data;
        private TreeNode<T> left, right;
        
        public TreeNode(T data) {
            this.data = data;
        }
        
        public void setLeft(TreeNode<T> left) {
            this.left = left;
        }
        
        public void setRight(TreeNode<T> right) {
            this.right = right;
        }
        
        public T data() {
            return data;
        }
        
        public TreeNode<T> left() {
            return left;
        }
        
        public TreeNode<T> right() {
            return right;
        }
    }
    
    public static String preOrderTraversal(TreeNode<Integer> root) {
        if(root == null) {
            return "";
        }
        
        return  root.data().toString() + preOrderTraversal(root.left()) + 
                preOrderTraversal(root.right());
    }
    
    public static void testPreOrderTraversal() {
        TreeNode<Integer> root = new TreeNode<Integer>(1);
        TreeNode<Integer> one = new TreeNode<Integer>(2);
        TreeNode<Integer> two = new TreeNode<Integer>(3);
        TreeNode<Integer> three = new TreeNode<Integer>(4);
        TreeNode<Integer> four = new TreeNode<Integer>(5);
        
        root.setLeft(one);
        root.setRight(two);
        one.setLeft(three);
        one.setRight(four);
        
        String result = preOrderTraversal(root).trim();
        assert "12453".equals(result);
    }
    
    public static String inOrderTraversal(TreeNode<Integer> root) {
        if(root == null) {
            return "";
        }
        
        return  inOrderTraversal(root.left()) + root.data().toString() +
                inOrderTraversal(root.right());
    }
    
    public static void testInOrderTraversal() {
        TreeNode<Integer> root = new TreeNode<Integer>(1);
        TreeNode<Integer> one = new TreeNode<Integer>(2);
        TreeNode<Integer> two = new TreeNode<Integer>(3);
        TreeNode<Integer> three = new TreeNode<Integer>(4);
        TreeNode<Integer> four = new TreeNode<Integer>(5);
        
        root.setLeft(one);
        root.setRight(two);
        one.setLeft(three);
        one.setRight(four);
        
        String result = inOrderTraversal(root).trim();
        assert "42513".equals(result);
    }
    
    public static String postOrderTraversal(TreeNode<Integer> root) {
        if(root == null) {
            return "";
        }
        
        return  postOrderTraversal(root.left()) + postOrderTraversal(root.right()) +
                root.data().toString();
    }
    
    public static void testPostOrderTraversal() {
        TreeNode<Integer> root = new TreeNode<Integer>(1);
        TreeNode<Integer> one = new TreeNode<Integer>(2);
        TreeNode<Integer> two = new TreeNode<Integer>(3);
        TreeNode<Integer> three = new TreeNode<Integer>(4);
        TreeNode<Integer> four = new TreeNode<Integer>(5);
        
        root.setLeft(one);
        root.setRight(two);
        one.setLeft(three);
        one.setRight(four);
        
        String result = postOrderTraversal(root).trim();
        assert "45231".equals(result);
    }
    
    public static String levelOrderTraversal(TreeNode<Integer> root) {
        String result = "";
        Queue<TreeNode<Integer>> queue = new LinkedList<TreeNode<Integer>>(); 
        queue.add(root);
        
        while(queue.peek() != null) {
            TreeNode<Integer> node = queue.poll();
            
            result += node.data().toString();
            
            if(node != null && node.left() != null) {
                queue.add(node.left());
            }
            if(node != null && node.right() != null) {
                queue.add(node.right());
            }
        }
        
        return result;
    }
    
    public static void testLevelOrderTraversal() {
        TreeNode<Integer> root = new TreeNode<Integer>(1);
        TreeNode<Integer> one = new TreeNode<Integer>(2);
        TreeNode<Integer> two = new TreeNode<Integer>(3);
        TreeNode<Integer> three = new TreeNode<Integer>(4);
        TreeNode<Integer> four = new TreeNode<Integer>(5);
        
        root.setLeft(one);
        root.setRight(two);
        one.setLeft(three);
        one.setRight(four);
        
        String result = levelOrderTraversal(root).trim();
        assert "12345".equals(result);
    }
    
    public static void main(String[] args) {
        testPreOrderTraversal();
        testInOrderTraversal();
        testPostOrderTraversal();
    }
}