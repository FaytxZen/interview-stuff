
public class Parity {
    
    public static int parityByShifting(long n) {
        int result = 0;
        
        while(n > 0) {
            
            result ^= (n & 1);
            n >>>= 1;
        }
        
        return result;
    }
    
    public static int parityByXor(long n) {
        int result = 0;
        
        while(n > 0) {
            result ^= 1;
            n &= n - 1;
        }
        
        return result;
    }
    
    public static void main(String[] args) {
        int case1 = 3245;
        int result1 = 1;
        
        int case2 = 9;
        int result2 = 0;
        
        int case3 = 7;
        int result3 = 1;
        
        int case4 = 1;
        int result4 = 1;
        
        int case5 = 0;
        int result5 = 0;
        
        int case6 = 64;
        int result6 = 1;
        
        System.out.println("Running tests for parityByShifting");
        int output1 = parityByShifting(case1);
        int output2 = parityByShifting(case2);
        int output3 = parityByShifting(case3);
        int output4 = parityByShifting(case4);
        int output5 = parityByShifting(case5);
        int output6 = parityByShifting(case6);
        
        assert result1 == output1 : "Case 1 failed";
        assert result2 == output2 : "Case 2 failed";
        assert result3 == output3 : "Case 3 failed";
        assert result4 == output4 : "Case 4 failed";
        assert result5 == output5 : "Case 5 failed";
        assert result6 == output6 : "Case 6 failed";
        
        System.out.println("\n");
        
        output1 = parityByXor(case1);
        output2 = parityByXor(case2);
        output3 = parityByXor(case3);
        output4 = parityByXor(case4);
        output5 = parityByXor(case5);
        output6 = parityByXor(case6);
        assert result1 == parityByXor(case1);
        assert result2 == parityByXor(case2);
        assert result3 == parityByXor(case3);
        assert result4 == parityByXor(case4);
        assert result5 == parityByXor(case5);
        assert result6 == parityByXor(case6);
    }
}