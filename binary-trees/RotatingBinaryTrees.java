
public class RotatingBinaryTrees {
    
    public static BinaryTree.BinaryTreeNode rotateRight(BinaryTree.BinaryTreeNode root) {
        if(root != null && height(root.left()) - height(root.right()) >= 1) {
            
            // save the reference to the left node
            BinaryTree.BinaryTreeNode left = root.left();
            
            // save the reference to the left's right sub-tree
            BinaryTree.BinaryTreeNode leftRightNode = left.right();
            
            // make the left the root
            root.setLeft(null);
            // make the left node's rightsubtree the root
            left.setRight(root);
            
            // add the left node's previous right subtree
            if(leftRightNode != null) {
                left.add(leftRightNode);
            }
            
            return left;
        }
        
        return root;
    }
    
    private static void rotateRight(BinaryTree tree) {
        tree.setRoot(rotateRight(tree.getRoot()));
    }
    
    public static void testRotateRight() {
        BinaryTree tree = new BinaryTree();
        tree.add(5);
        tree.add(6);
        tree.add(3);
        tree.add(4);
        tree.add(2);
        tree.add(1);
        
        BinaryTree expectedTree = new BinaryTree();
        expectedTree.add(3);
        expectedTree.add(2);
        expectedTree.add(1);
        expectedTree.add(5);
        expectedTree.add(4);
        expectedTree.add(6);
        
        rotateRight(tree);
        assert tree.inOrder().equals(expectedTree.inOrder());
        
        tree = new BinaryTree();
        tree.add(3);
        tree.add(2);
        tree.add(1);
        
        expectedTree = new BinaryTree();
        expectedTree.add(2);
        expectedTree.add(1);
        expectedTree.add(3);
        
        rotateRight(tree);
        assert tree.inOrder().equals(expectedTree.inOrder());
    }
    
    public static BinaryTree.BinaryTreeNode rotateLeft(BinaryTree.BinaryTreeNode root) {
        
        if(root != null && height(root.right()) - height(root.left()) >= 1) {
            
            BinaryTree.BinaryTreeNode right = root.right();
            BinaryTree.BinaryTreeNode rightLeftNode = right.left();
            
            root.setRight(null);
            right.setLeft(root);
            
            if(rightLeftNode != null) {
                right.add(rightLeftNode);
            }
            
            return right;
        }
        
        return root;
    }
    
    public static void rotateLeft(BinaryTree tree) {
        tree.setRoot(rotateLeft(tree.getRoot()));
    }
    
    public static void testRotateLeft() {
        BinaryTree tree = new BinaryTree();
        tree.add(5);
        tree.add(8);
        tree.add(9);
        
        BinaryTree expectedTree = new BinaryTree();
        expectedTree.add(8);
        expectedTree.add(5);
        expectedTree.add(9);
        
        rotateLeft(tree);
        assert tree.inOrder().equals(expectedTree.inOrder());
        
        tree = new BinaryTree();
        tree.add(5);
        tree.add(1);
        tree.add(0);
        tree.add(8);
        tree.add(7);
        tree.add(10);
        tree.add(9);
        tree.add(11);
        tree.add(12);
        
        expectedTree = new BinaryTree();
        expectedTree.add(8);
        expectedTree.add(5);
        expectedTree.add(1);
        expectedTree.add(0);
        expectedTree.add(7);
        expectedTree.add(10);
        expectedTree.add(9);
        expectedTree.add(11);
        expectedTree.add(12);
        
        rotateLeft(tree);
        assert tree.inOrder().equals(expectedTree.inOrder());
    }
    
    public static void rotateRightLeft(BinaryTree tree) {
        BinaryTree.BinaryTreeNode root = tree.getRoot();
        
        if(root != null && height(root.right()) - height(root.left()) >= 2) {
            root.setRight(rotateRight(root.right()));
        
            tree.setRoot(rotateLeft(root));
        }
    }
    
    public static void testRotateRightLeft() {
        BinaryTree tree = new BinaryTree();
        tree.add(2);
        tree.add(4);
        tree.add(3);
        
        BinaryTree expectedTree = new BinaryTree();
        expectedTree.add(3);
        expectedTree.add(2);
        expectedTree.add(4);
        
        rotateRightLeft(tree);
        assert tree.inOrder().equals(expectedTree.inOrder());
        
        tree = new BinaryTree();
        tree.add(5);
        tree.add(4);
        tree.add(10);
        tree.add(9);
        tree.add(7);
        tree.add(6);
        tree.add(8);
        
        expectedTree = new BinaryTree();
        expectedTree.add(9);
        expectedTree.add(10);
        expectedTree.add(5);
        expectedTree.add(4);
        expectedTree.add(7);
        expectedTree.add(6);
        expectedTree.add(8);
        
        rotateRightLeft(tree);
        assert tree.inOrder().equals(expectedTree.inOrder());
    }
    
    public static void rotateLeftRight(BinaryTree tree) {
        BinaryTree.BinaryTreeNode root = tree.getRoot();
        
        if(root != null && height(root.left()) - height(root.right()) >= 2) {
            root.setLeft(rotateLeft(root.left()));
            
            tree.setRoot(rotateRight(root));
        }
    }
    
    public static void testRotateLeftRight() {
        BinaryTree tree = new BinaryTree();
        tree.add(4);
        tree.add(2);
        tree.add(3);
        
        BinaryTree expectedTree = new BinaryTree();
        expectedTree.add(3);
        expectedTree.add(2);
        expectedTree.add(4);
        
        rotateLeftRight(tree);
        assert tree.inOrder().equals(expectedTree.inOrder());
        
        tree = new BinaryTree();
        tree.add(5);
        tree.add(10);
        tree.add(2);
        tree.add(4);
        tree.add(3);
        
        expectedTree = new BinaryTree();
        expectedTree.add(4);
        expectedTree.add(2);
        expectedTree.add(3);
        expectedTree.add(5);
        expectedTree.add(10);
        
        rotateLeftRight(tree);
        assert tree.inOrder().equals(expectedTree.inOrder());
    }
    
    public static int height(BinaryTree.BinaryTreeNode node) {
        if(node == null) {
            return 0;
        }
        else {
            return 1 + (int) Math.max(height(node.left()), height(node.right())); 
        }
    }
    
    public static void testHeight() {
        BinaryTree tree = new BinaryTree();
        tree.add(4);
        tree.add(3);
        tree.add(2);
        tree.add(1);
        
        assert height(tree.getRoot()) == 4;
        
        tree = new BinaryTree();
        tree.add(1);
        
        assert height(tree.getRoot()) == 1;
        
        tree = new BinaryTree();
        tree.add(5);
        tree.add(2);
        tree.add(6);
        tree.add(7);
        tree.add(8);
        
        assert height(tree.getRoot()) == 4;
    }
    
    public static void main(String[] args) {
        testRotateRight();
        testRotateLeft();
        testRotateLeftRight();
        testRotateRightLeft();
        
        testHeight();
    }
}