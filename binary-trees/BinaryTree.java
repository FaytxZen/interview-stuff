import java.util.*;

public class BinaryTree {
    
    private BinaryTreeNode root;
    
    public static class BinaryTreeNode {
        
        private int data;
        
        private BinaryTreeNode right;
        private BinaryTreeNode left;
        
        public BinaryTreeNode(int data) {
            this.data = data;
        }
        
        public BinaryTreeNode right() {
            return right;
        }
        
        public void setRight(BinaryTreeNode right) {
            this.right = right;
        }
        
        public BinaryTreeNode left() {
            return left;
        }
        
        public void setLeft(BinaryTreeNode left) {
            this.left = left;
        }
        
        public int data() {
            return data;
        }
        
        public void add(int data) {
            add(new BinaryTreeNode(data));
        }
    
        public void add(BinaryTreeNode node) {
            BinaryTreeNode current = this;
            
            while(current != null) {
                if(current.data() > node.data()) {
                    if(current.left() == null) {
                        current.setLeft(node);
                        break;
                    }
                    else {
                        current = current.left();
                    }
                }
                else {
                    if(current.right() == null) {
                        current.setRight(node);
                        break;
                    }
                    else {
                        current = current.right();
                    }
                }
            }
        }
    }
    
    public void add(int data) {
        if(root == null) {
            root = new BinaryTreeNode(data);
        }
        else {
            root.add(new BinaryTreeNode(data));    
        }
    }
    
    public void add(BinaryTreeNode node) {
        if(root == null) {
            root = node;
        }
        else {
            root.add(node);
        }
    }
    
    public BinaryTreeNode find(int data) {
        BinaryTreeNode current = root;
        
        while(current != null) {
            if(data == current.data()) {
                break;
            }
            else if(data < current.data()) {
                current = current.left(); 
            }
            else {
                current = current.right();
            }
        }
        
        return current;
    }
    
    public BinaryTreeNode getRoot() {
        return root;
    }
    
    public void setRoot(BinaryTreeNode root) {
        this.root = root;
    }
    
    public String toString() {
        Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
        queue.add(root);
        
        String result = "";
        
        while(queue.peek() != null) {
            BinaryTreeNode node = queue.poll();
            
            result += node.data();
            
            if(node.left() != null) {
                queue.add(node.left());
            }
            if(node.right() != null) {
                queue.add(node.right());
            }
        }
        
        return result;
    }
    
    public String inOrder() {
        return inOrder(root);
    }
    
    public String inOrder(BinaryTreeNode root) {
        if(root == null) {
            return "";
        }
        else {
            return root.data() + inOrder(root.left()) + inOrder(root.right());
        }
    }
    
    public String levelOrder() {
        String result = "";
        Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>(); 
        queue.add(root);
        
        while(queue.peek() != null) {
            BinaryTreeNode node = queue.poll();
            
            result += node.data();
            
            if(node != null && node.left() != null) {
                queue.add(node.left());
            }
            if(node != null && node.right() != null) {
                queue.add(node.right());
            }
        }
        
        return result;
    }
    
    public static void testBinaryTree() {
        BinaryTree tree = new BinaryTree();
        tree.add(6);
        tree.add(3);
        tree.add(2);
        tree.add(8);
        tree.add(7);
        
        assert "63827".equals(tree.toString());
        
        assert tree.find(0) == null;
        assert tree.find(3).data() == 3;
    }
    
    public static void testBinaryTreeNode() {
        BinaryTree tree = new BinaryTree();
        tree.add(3);
        
        BinaryTreeNode root = tree.getRoot();
        root.add(2);
        root.add(1);
        root.add(4);
        
        assert "3241".equals(tree.toString());
    }
    
    public static void main(String[] args) {
        testBinaryTree();
    }
}