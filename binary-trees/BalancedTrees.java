
import java.util.*;
import java.lang.*;

public class BalancedTrees {
    
    public static boolean isBalanced(BinaryTree tree) {
        int leftHeight = height(tree.getRoot().left());
        int rightHeight = height(tree.getRoot().right());
        
        return Math.abs(leftHeight - rightHeight) <= 1;
    }
    
    public static int height(BinaryTree tree) {
        return height(tree.getRoot());
    }
    
    public static int height(BinaryTree.BinaryTreeNode root) {
        if(root == null) {
            return 0;
        }
        
        else return 1 + Math.max(height(root.left()), height(root.right()));
    }
    
    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree();
        tree.add(1);
        tree.add(2);
        tree.add(3);
        tree.add(4);
        
        assert height(tree) == 4;
        assert !isBalanced(tree);
        
        tree = new BinaryTree();
        tree.add(3);
        tree.add(2);
        tree.add(4);
        tree.add(1);
        tree.add(0);
        
        assert height(tree) == 4;
        assert !isBalanced(tree);
        
        tree = new BinaryTree();
        tree.add(10);
        tree.add(3);
        tree.add(4);
        tree.add(11);
        tree.add(13);
        
        assert height(tree) == 3;
        assert isBalanced(tree);
    }
}