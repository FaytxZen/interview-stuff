import java.util.*;

public class Trie {
    
    private static final int NUM_LETTERS = 26;
    
    private int words;
    private int prefixes;
    private char prev;
    
    private Trie[] children;
    
    public Trie() {
        words = 0;
        prefixes = 0;
        prev = 0;
        
        children = new Trie[NUM_LETTERS];
    }
    
    public int words() {
        return words;
    }
    
    public void setWords(int count) {
        words = count;
    }
    
    public int prefixes() {
        return prefixes;
    }
    
    public void setPrefixes(int count) {
        prefixes = count;
    }
    
    public void setPrev(char prev) {
        this.prev = prev;
    }
    
    public char prev() {
        return prev;
    }
    
    public Trie[] children() {
        return children;
    }
    
    public void addWord(String word) {
        addWord(this, word);
    }
    
    public static void addWord(Trie vertex, String word) {
        if(word.isEmpty()) {
            vertex.setWords(vertex.words()+1);
        }
        else {
            vertex.setPrefixes(vertex.prefixes()+1);
            
            char first = word.charAt(0);
            int index = first - 'a';
            
            if(vertex.children()[index] == null) {
                vertex.children()[index] = new Trie();
                vertex.children()[index].setPrev(first);
            }
            
            String newWord = word.substring(1, word.length());
            addWord(vertex.children()[index], newWord);
        }
    }
    
    public int countPrefixes(String prefix) {
        return countPrefixes(this, prefix);
    }
    
    public static int countPrefixes(Trie vertex, String prefix) {
        if(prefix.isEmpty()) {
            return vertex.prefixes();
        }
        else if(vertex.children()[prefix.charAt(0)-'a'] == null) {
            return 0;
        }
        else {
            char first = prefix.charAt(0);
            Trie nextVertex = vertex.children()[first-'a'];
            
            String newWord = prefix.substring(1, prefix.length());
            
            return countPrefixes(nextVertex, newWord);
        }
    }
    
    public int countWords(String word) {
        return countWords(this, word);
    }
    
    public static int countWords(Trie vertex, String word) {
        if(word.isEmpty()) {
            return vertex.words();
        }
        
        char first = word.charAt(0);
        if(vertex.children()[first-'a'] == null) {
            return 0;
        }
        else {
            Trie nextVertex = vertex.children()[first-'a'];
            
            String newString = word.substring(1, word.length());
            
            return countWords(nextVertex, newString);
        }
    }
    
    public static void levelOrder(Trie root) {
        Queue<Trie> queue = new LinkedList<Trie>();
        queue.add(root);
        
        while(queue.peek() != null) {
            Trie front = queue.remove();
            System.out.print(front.prev());
            
            for(Trie child : front.children()) {
                if(child != null) {
                    queue.add(child);
                }
            }
        }
    }
    
    public static void main(String[] args) {
        Trie root = new Trie();
        root.addWord("he");
        root.addWord("car");
        root.addWord("carnivore");
        root.addWord("hell");
        root.addWord("hello");
        
        assert root.countWords("hello") == 1;
        assert root.countPrefixes("he") == 2;
        assert root.countWords("carnivore") == 1;
        assert root.countPrefixes("car") == 1;
    }
}