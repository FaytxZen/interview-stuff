
public class SinglyLinkedListProblems {
    
    public static <T> void deleteDuplicates(SinglyLinkedList.Node<T> root) {
        SinglyLinkedList.Node<T> current = root;
        
        while(current != null && current.getNext() != null) {
            SinglyLinkedList.Node<T> node = current;
            
            while(node != null && node.getNext() != null) {
                SinglyLinkedList.Node<T> next = node.getNext();
                
                if(current.getData().equals(next.getData())) {
                    node.setNext(next.getNext());
                }
                else {
                    node = next;
                }
            }
            
            current = current.getNext();
        }
    }
    
    public static void testDeleteDuplicates() {
        // case 1
        SinglyLinkedList<Integer> linkedList = new SinglyLinkedList<Integer>();
        linkedList.add(1);
        linkedList.add(1);
        linkedList.add(2);
        
        deleteDuplicates(linkedList.getRoot());
        assert "1->2".equals(linkedList.toString());
        
        // case 2
        linkedList = new SinglyLinkedList<Integer>();
        linkedList.add(12);
        linkedList.add(12);
        
        deleteDuplicates(linkedList.getRoot());
        assert "12".equals(linkedList.toString());
        
        // case 3
        linkedList = new SinglyLinkedList<Integer>();
        linkedList.add(6);
        linkedList.add(8);
        linkedList.add(12);
        
        deleteDuplicates(linkedList.getRoot());
        assert "6->8->12".equals(linkedList.toString());
        
        // case 4
        linkedList = new SinglyLinkedList<Integer>();
        linkedList.add(0);
        
        deleteDuplicates(linkedList.getRoot());
        assert "0".equals(linkedList.toString());
    }
    
    public static <T> SinglyLinkedList.Node<T> findNthFromLastNode(SinglyLinkedList.Node<T> root, int n) {
        SinglyLinkedList.Node<T> current = root;
        
        int sizeOfList = 0;
        while(current != null) {
            current = current.getNext();
            
            sizeOfList++;
        }
        
        int curIndex = 0;
        current = root;
        while(current != null) {
            if(curIndex == sizeOfList - 1 - n) {
                break;
            }
            
            curIndex++;
            current = current.getNext();
        }
        
        return current;
    }
    
    public static void testFindNthFromLastNode() {
        SinglyLinkedList<Integer> linkedList = new SinglyLinkedList<Integer>();
        linkedList.add(1);
        linkedList.add(2);
        linkedList.add(4);
        linkedList.add(7);
        
        SinglyLinkedList.Node<Integer> node = findNthFromLastNode(linkedList.getRoot(), 1);
        assert node.getData().equals(4);
        
        node = findNthFromLastNode(linkedList.getRoot(), 2);
        assert node.getData().equals(2);
        
        linkedList = new SinglyLinkedList<Integer>();
        linkedList.add(1);
        
        node = findNthFromLastNode(linkedList.getRoot(), 0);
        assert node.getData().equals(1);
    }
    
    public static <T> void deleteThisNode(SinglyLinkedList.Node<T> node) {
        if(node != null && node.getNext() != null) {
            SinglyLinkedList.Node<T> next = node.getNext();
            
            node.setData(next.getData());
            node.setNext(next.getNext());
        }
    }
    
    public static void testDeleteThisNode() {
        SinglyLinkedList<Integer> linkedList = new SinglyLinkedList<Integer>();
        linkedList.add(15);
        linkedList.add(1);
        
        deleteThisNode(linkedList.getRoot());
        assert "1".equals(linkedList.getRoot().toString());
        
        linkedList = new SinglyLinkedList<Integer>();
        linkedList.add(0);
        linkedList.add(60);
        linkedList.add(4);
        
        deleteThisNode(linkedList.getRoot().getNext());
        assert "0->4".equals(linkedList.getRoot().toString());
    }
    
    public static SinglyLinkedList<Integer> addLinkedListValues ( 
                                            SinglyLinkedList<Integer> l1,
                                            SinglyLinkedList<Integer> l2 )
    {
        SinglyLinkedList.Node<Integer> current1 = l1.getRoot();
        SinglyLinkedList.Node<Integer> current2 = l2.getRoot();
        
        String num1Str = "";
        String num2Str = "";
        
        while(current1 != null || current2 != null) {
            if(current1 != null) {
                num1Str += current1.getData();
                current1 = current1.getNext();
            }
            
            if(current2 != null) {
                num2Str += current2.getData();
                current2 = current2.getNext();
            }
        }
        
        SinglyLinkedList<Integer> sumList = new SinglyLinkedList<Integer>();
        Integer sum = Integer.parseInt(num1Str) + Integer.parseInt(num2Str);
        String sumStr = sum.toString();
        
        for(int i = 0; i < sumStr.length(); i++) {
            sumList.add(sumStr.charAt(i) - '0');
        }
        
        return sumList;
    }
    
    public static void testAddLinkedListValues() {
        // case 1
        SinglyLinkedList<Integer> list1 = new SinglyLinkedList<Integer>();
        list1.add(1);
        list1.add(2);
        list1.add(5);
        
        SinglyLinkedList<Integer> list2 = new SinglyLinkedList<Integer>();
        list2.add(7);
        list2.add(8);
        list2.add(9);
        
        SinglyLinkedList<Integer> sum = addLinkedListValues(list1, list2);
        assert "9->1->4".equals(sum.toString());
        
        // case 2
        list1 = new SinglyLinkedList<Integer>();
        list1.add(1);
        list1.add(0);
        list1.add(0);
        
        list2 = new SinglyLinkedList<Integer>();
        list2.add(2);
        list2.add(5);
        sum = addLinkedListValues(list1, list2);
        assert "1->2->5".equals(sum.toString());
        
        // case 3
        list1 = new SinglyLinkedList<Integer>();
        list1.add(0);
        
        list2 = new SinglyLinkedList<Integer>();
        list2.add(0);
        
        sum = addLinkedListValues(list1, list2);
        assert "0".equals(sum.toString());
    }
    
    public static void main(String[] args) {
        testDeleteDuplicates();
        
        testFindNthFromLastNode();
        
        testDeleteThisNode();
        
        testAddLinkedListValues();
    }
}