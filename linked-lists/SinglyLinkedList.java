
public class SinglyLinkedList<T> {
    
    public static class Node<T> {
        
        private T data;
        private Node<T> next;
        
        public Node(T data) {
            this.data = data;
        }
        
        @Override
        public String toString() {
            Node<T> head = this;
            String str = "";
            
            while(head != null) {
                String arrow = head.getNext() != null ? "->" : "";
                str += String.format("%s%s", head.getData().toString(), arrow);
                
                head = head.getNext();
            }
            
            return str;
        }
        
        public Node<T> add(Node<T> node) {
            
            Node<T> head = this;
            
            while(head.getNext() != null) {
                head = head.getNext();
            }
            
            head.setNext(node);
            
            return this;
        }
        
        public Node<T> add(T data) {
            return add(new Node<T>(data));
        }
        
        public Node<T> delete(T data) {
            Node<T> head = this;
            
            while(head.getNext() != null) {
                if(head.getNext().getData() == data) {
                    head.setNext(head.getNext().getNext());
                    break;
                }
                else {
                    head = head.getNext();
                }
            }
            
            return head.getData() == data ? null : head;
        }
        
        public Node<T> find(T data) {
            Node<T> head = this;
            
            while(head != null) {
                if(head.getData() != data) {
                    head = head.getNext();
                }
                else {
                    break;
                }
            }
            
            return head;
        }
        
        public T getData() {
            return data;
        }
        
        public Node<T> getNext() {
            return next;
        }
        
        public void setData(T data) {
            this.data = data;
        }
        
        public void setNext(Node<T> node) {
            this.next = node;
        }
    }
    
    private Node<T> root;
    
    public Node<T> getRoot() {
        return root;
    }
    
    public void add(T data) {
        if(root == null) {
            root = new Node<T>(data);
        }
        else {
            root.add(data);
        }
    }
    
    public void delete(T data) {
        if(root != null) {
            root = root.delete(data);
        }
    }
    
    public Node<T> find(T data) {
        if(root != null) {
            return root.find(data);
        }
        else {
            return null;
        }
    }
    
    @Override
    public String toString() {
        if(root != null) {
            return root.toString();
        }
        else {
            return "";
        }
    }
    
    public static void main(String[] args) {
        Node<Integer> head = new Node<Integer>(4);
        head.add(12);
        head.add(1);
        
        assert head.toString().equals("4->12->1");
        
        Node<Integer> foundNode = head.find(1);
        assert foundNode.getData() == 1;
        
        head = head.delete(12);
        assert head.toString().equals("4->1");
        
        head = head.delete(4);
        assert head.toString().equals("1");
    }
}